import * as USVFS from './node-usvfs';
import { resolve as resolveLocation, join as joinLocation} from 'path';
import * as fs from 'fs';
import * as config from './config.json';
import { Command } from 'commander';
import rimraf from 'rimraf';
import colors = require('colors');

const cli = new Command();

cli
  .name('tML Profile Manager [usvfs]')
  .version('0.1');

cli
  .command('create <name>')
  .description(colors.cyan('Creating profile.'))
  .action(createNewProfile);

function createNewProfile(name: string): void {
  if(!name) throw new Error(colors.red('Profile name must be specified!'));
  if(!fs.existsSync(config.profilesLocation)) throw new Error(colors.red('Profiles locations not exists!'));

  const profileLocation = joinLocation(config.profilesLocation, name);
  if(fs.existsSync(profileLocation)) throw new Error(colors.red(`Profile with name ${name} already exists!`));

  fs.mkdirSync(profileLocation);
  console.log(colors.yellow(`Profile ${name} created.`));
}

cli
  .command('remove <profile>')
  .description(colors.cyan('Removing profile.'))
  .action(removeProfile);

function removeProfile(name: string): void {
  if(!name) throw new Error(colors.red('Profile name must be specified!'));
  if(!fs.existsSync(config.profilesLocation)) throw new Error(colors.red('Profiles locations not exists!'));
  const profileLocation = joinLocation(config.profilesLocation, name);
  rimraf.sync(profileLocation);
  console.log(colors.yellow(`Profile ${name} removed.`));
}

cli
  .command('list')
  .description(colors.cyan('List all profiles.'))
  .action(listProfiles);
// TODO: rework with jsonDB
function listProfiles(): void {
  if(!fs.existsSync(config.profilesLocation)) throw new Error(colors.red('Profiles locations not exists!'));
  const dirs = fs.readdirSync(config.profilesLocation);
  let message = colors.yellow('Profiles:\n\t');
  message += colors.white(dirs.join('\n\t'));
  console.log(message)
}

cli
  .command('start-client <profile>')
  .description(colors.cyan('Starting tML Client.'))
  .action(startClient);

function startClient(profile: string){
  if(!fs.existsSync(config.profilesLocation)) throw new Error(colors.red('Profiles locations not exists!'));
  const profileLocation = joinLocation(config.profilesLocation, profile);
  if(!fs.existsSync(profileLocation)) throw new Error(colors.red(`Profile with name ${profile} not exists!`));

  USVFS.CreateVFS({
    instanceName: 'Terraria',
    debugMode: false,
    logLevel: 0,
    crashDumpType: 0,
    crashDumpPath: resolveLocation(__dirname, 'logs')
  });
  console.log(resolveLocation(config.my_gamesLocation, 'Terraria'))
  //USVFS.ClearVirtualMappings();
  USVFS.VirtualLinkDirectoryStatic(resolveLocation(profileLocation), resolveLocation(config.my_gamesLocation, 'Terraria'),{
    failIfExists: false,
    monitorChanges: true,
    createTarget: true,
    recursive: true,
  });
  //USVFS.CreateProcessHooked('', resolveLocation(config.tMLLocation, 'Terraria.exe'), resolveLocation(config.tMLLocation), null);
  USVFS.CreateProcessHooked('', 'F:\\Explorer++.exe', resolveLocation(config.tMLLocation), null);
}

cli.parse(process.argv);






